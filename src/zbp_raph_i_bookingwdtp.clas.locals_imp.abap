CLASS lhc_booking DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.

    METHODS calculatetotalprice FOR DETERMINE ON MODIFY
      IMPORTING keys FOR booking~calculatetotalprice.

    METHODS setbookingdate FOR DETERMINE ON MODIFY
      IMPORTING keys FOR booking~setbookingdate.

    METHODS validatecarrier FOR VALIDATE ON SAVE
      IMPORTING keys FOR booking~validatecarrier.

    METHODS validatecustomer FOR VALIDATE ON SAVE
      IMPORTING keys FOR booking~validatecustomer.

ENDCLASS.

CLASS lhc_booking IMPLEMENTATION.

  METHOD calculatetotalprice.
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY booking BY \_travel
        FROM CORRESPONDING #( keys )
      LINK DATA(lt_link).

    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
      EXECUTE recalctotalprice
      FROM VALUE #( FOR <link> IN lt_link ( COND #( WHEN <link>-target-%is_draft = if_abap_behv=>mk-off THEN VALUE #( traveluuid = <link>-target-traveluuid ) ) ) )
      REPORTED DATA(lt_reported).

    reported = CORRESPONDING #( DEEP lt_reported ).
  ENDMETHOD.

  METHOD setbookingdate.
    " Read relevant booking instances
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY booking
        FIELDS ( bookingdate )
        WITH CORRESPONDING #( keys )
      RESULT DATA(lt_booking).

    " Loop over bookings and set system date as booking date
    LOOP AT lt_booking ASSIGNING FIELD-SYMBOL(<ls_booking>) WHERE bookingdate IS INITIAL.
      <ls_booking>-bookingdate = cl_abap_context_info=>get_system_date( ).
    ENDLOOP.

    " Save changes via EML
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY booking
        UPDATE FIELDS ( bookingdate )
        WITH CORRESPONDING #( lt_booking )
      REPORTED DATA(lt_reported).

    " Forward reported instances
    reported = CORRESPONDING #( DEEP lt_reported ).
  ENDMETHOD.

  METHOD validatecarrier.
    " Read relevant travel instance data
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
         ENTITY booking
           FIELDS (  carrierid )
           WITH CORRESPONDING #( keys )
       RESULT DATA(lt_booking)
       FAILED DATA(lt_failed).

    " Forward failed instances
    failed = CORRESPONDING #( DEEP lt_failed ).

    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY booking BY \_travel
        FROM CORRESPONDING #( lt_booking )
      LINK DATA(lt_link).

    " Get Carrier Master Data for comparison
    SELECT FROM /dmo/carrier AS carrier
    INNER JOIN @lt_booking AS booking ON booking~carrierid = carrier~carrier_id
    FIELDS carrier_id
    INTO TABLE @DATA(lt_carrier_db).

    " Loop over all travel instances to be saved
    LOOP AT lt_booking INTO DATA(ls_booking).
      APPEND VALUE #(  %tky                = ls_booking-%tky
                       %state_area         = 'VALIDATE_CARRIER' ) TO reported-booking.

      " Raise messages for empty carrier ids
      IF ls_booking-carrierid IS  INITIAL.
        APPEND VALUE #( %tky = ls_booking-%tky ) TO failed-booking.
        APPEND VALUE #( %tky               = ls_booking-%tky
                        %state_area        = 'VALIDATE_CARRIER'
                        %msg               = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                          number   = '009' " Carrier is initial
                                                          severity = if_abap_behv_message=>severity-error )
                        %path              = VALUE #( travel-%tky = lt_link[ KEY draft COMPONENTS source-%tky = ls_booking-%tky ]-target-%tky )
                        %element-carrierid = if_abap_behv=>mk-on ) TO reported-booking.

        " Raise messages for non-existing carrier ids
      ELSEIF ls_booking-carrierid IS NOT INITIAL AND NOT line_exists( lt_carrier_db[ carrier_id = ls_booking-carrierid ] ).
        APPEND VALUE #( %tky = ls_booking-%tky ) TO failed-booking.
        APPEND VALUE #( %tky               = ls_booking-%tky
                        %state_area        = 'VALIDATE_CARRIER'
                        %msg               = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                          number   = '010' " Carrier &1 unknown
                                                          v1       = ls_booking-carrierid
                                                          severity = if_abap_behv_message=>severity-error )
                        %path              = VALUE #( travel-%tky = lt_link[ KEY draft COMPONENTS source-%tky = ls_booking-%tky ]-target-%tky )
                        %element-carrierid = if_abap_behv=>mk-on ) TO reported-booking.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.

  METHOD validatecustomer.
    " Read relevant travel instance data
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
         ENTITY booking
           FIELDS (  customerid )
           WITH CORRESPONDING #( keys )
       RESULT DATA(lt_booking)
       FAILED DATA(lt_failed).

    " Forward failed instances
    failed = CORRESPONDING #( DEEP lt_failed ).

    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY booking BY \_travel
        FROM CORRESPONDING #( lt_booking )
      LINK DATA(lt_link).

    " Get Customer Master Data for comparison
    SELECT FROM /dmo/customer AS md_customer
    INNER JOIN @lt_booking AS book ON book~customerid = md_customer~customer_id
    FIELDS customer_id
    INTO TABLE @DATA(lt_customer_db).

    " Loop over all travel instances to be saved
    LOOP AT lt_booking INTO DATA(ls_booking).
      APPEND VALUE #(  %tky               = ls_booking-%tky
                       %state_area        = 'VALIDATE_CUSTOMER' ) TO reported-booking.

      " Raise messages for empty customer ids
      IF ls_booking-customerid IS  INITIAL.
        APPEND VALUE #( %tky = ls_booking-%tky ) TO failed-booking.
        APPEND VALUE #( %tky                = ls_booking-%tky
                        %state_area         = 'VALIDATE_CUSTOMER'
                        %msg                = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '005' " Customer is initial
                                                           severity = if_abap_behv_message=>severity-error )
                        %path               = VALUE #( travel-%tky = lt_link[ KEY draft COMPONENTS source-%tky = ls_booking-%tky ]-target-%tky )
                        %element-customerid = if_abap_behv=>mk-on ) TO reported-booking.

        " Raise messages for non existing customer ids
      ELSEIF ls_booking-customerid IS NOT INITIAL AND NOT line_exists( lt_customer_db[ customer_id = ls_booking-customerid ] ).
        APPEND VALUE #(  %tky = ls_booking-%tky ) TO failed-booking.
        APPEND VALUE #( %tky                = ls_booking-%tky
                        %state_area         = 'VALIDATE_CUSTOMER'
                        %msg                = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '006' " Customer &1 unknown
                                                           v1       = ls_booking-customerid
                                                           severity = if_abap_behv_message=>severity-error )
                        %path               = VALUE #( travel-%tky = lt_link[ KEY draft COMPONENTS source-%tky = ls_booking-%tky ]-target-%tky )
                        %element-customerid = if_abap_behv=>mk-on ) TO reported-booking.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.

ENDCLASS.
