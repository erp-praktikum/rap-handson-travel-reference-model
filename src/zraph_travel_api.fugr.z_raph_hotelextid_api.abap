FUNCTION z_raph_hotelextid_api.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  EXPORTING
*"     REFERENCE(ET_HOTELHEADER) TYPE  ZRAPH_T_HOTELEXTID
*"----------------------------------------------------------------------
  .

  et_hotelheader = VALUE #(
   ( client ='100' hotel_external_id ='000001' hotel_name ='Mothership' city ='Cupertino' country ='US' )
   ( client ='100' hotel_external_id ='000002' hotel_name ='Grand Hotel' city ='Washington' country ='US'  )
   ( client ='100' hotel_external_id ='000003' hotel_name ='British Yard' city ='London' country ='UK'  )
   ( client ='100' hotel_external_id ='000004' hotel_name ='Trump Hotel' city ='New York' country ='US'  )
   ( client ='100' hotel_external_id ='000005' hotel_name ='Sunny Side' city ='Barcelona' country ='ES'  )
   ( client ='100' hotel_external_id ='000006' hotel_name ='Sunrise Beach' city ='San Francisco' country ='US'  )
   ( client ='100' hotel_external_id ='000007' hotel_name ='Sushi Palace' city ='Kyoto' country ='JP'  )
   ( client ='100' hotel_external_id ='000008' hotel_name ='Sunshine Travel' city ='Munich' country ='DE'  )
   ( client ='100' hotel_external_id ='000009' hotel_name ='Hotel Santa Chiara' city ='Venice' country ='IT'  )
   ( client ='100' hotel_external_id ='000010' hotel_name ='Mediterranean Palace' city ='Rome' country ='IT'  )
   ).

ENDFUNCTION.
