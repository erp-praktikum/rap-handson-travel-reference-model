@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'RAP HandsOn: Room Reservation (Basic)'
@Metadata.ignorePropagatedAnnotations: true
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}

define view entity ZRAPH_I_RoomReservationWD
  as select from zraph_a_room_rsv
{
  room_rsv_uuid         as RoomResvnUUID,
  parent_uuid           as TravelUUID,
  room_rsv_id           as RoomResvnID,
  hotel_id              as HotelID,
  begin_date            as BeginDate,
  end_date              as EndDate,
  room_type             as RoomType,
  @Semantics.amount.currencyCode: 'CurrencyCode'
  room_rsv_price        as RoomResvnPrice,
  currency_code         as CurrencyCode,
  local_last_changed_at as LocalLastChangedAt
}
