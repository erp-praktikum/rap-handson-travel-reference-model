@EndUserText.label: 'Hotel External Id'
@ObjectModel.query.implementedBy: 'ABAP:ZRAPH_CL_HOTELEXTID'

define custom entity zraph_ce_hotelextid
{
  key hotel_external_id : zraph_hotel_ext_id;
      hotel_name        : zraph_hotel_name;
}
