@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'RAP HandsOn: Travel (ALP)'

@Metadata.allowExtensions: true

@ObjectModel.semanticKey: ['TravelID']

define view entity ZRAPH_C_Travel_ALP
  as select from ZRAPH_I_TravelWDTP
{
  key TravelUUID,

      TravelID,

      @ObjectModel.text.element: ['AgencyName']
      @Consumption.valueHelpDefinition: [{ entity : {name: '/DMO/I_Agency', element: 'AgencyID'  } }]
      AgencyID,
      _Agency.Name            as AgencyName,

      @ObjectModel.text.element: ['CustomerName']
      @Consumption.valueHelpDefinition: [{ entity : {name: '/DMO/I_Customer', element: 'CustomerID'  } }]
      CustomerID,
      _Customer.LastName      as CustomerName,


      BeginDate,

      EndDate,

      BookingFee,

      @Aggregation.default: #SUM
      TotalPrice,

      @Consumption.valueHelpDefinition: [{entity: {name: 'I_Currency', element: 'Currency' }}]
      CurrencyCode,

      Description,

      @Consumption.valueHelpDefinition: [{entity: { name: 'ZRAPH_I_OverallStatus', element: 'TravelStatus' } }]
      @ObjectModel.foreignKey.association: '_TravelStatus'
      OverallStatus,

      @Aggregation.referenceElement: ['CustomerID']
      @Aggregation.default: #COUNT_DISTINCT
      cast( 1 as abap.int4 ) as Customer,

   


      /* Associations */
      _Agency,
      _Booking,
      _Currency,
      _Customer,
      _RoomReservation,
      _TravelStatus
}
