@EndUserText.label: 'RAP HandsOn: Travel Projection View'
@AccessControl.authorizationCheck: #NOT_REQUIRED

@Metadata.allowExtensions: true
@Search.searchable: true
@ObjectModel.semanticKey: ['TravelID']

define root view entity ZRAPH_C_TravelWDTP
  provider contract transactional_query
  as projection on ZRAPH_I_TravelWDTP
{
  key     TravelUUID,

          @Search.defaultSearchElement: true
          TravelID,

          @Search.defaultSearchElement: true
          @Search.fuzzinessThreshold: 0.7
          @ObjectModel.text.element: ['AgencyName']
          @Consumption.valueHelpDefinition: [{ entity : {name: '/DMO/I_Agency', element: 'AgencyID'  } }]
          AgencyID,
          _Agency.Name       as AgencyName,

          @Search.defaultSearchElement: true
          @Search.fuzzinessThreshold: 0.7
          @ObjectModel.text.element: ['CustomerName']
          @Consumption.valueHelpDefinition: [{ entity : {name: '/DMO/I_Customer', element: 'CustomerID'  } }]
          CustomerID,
          _Customer.LastName as CustomerName,

          BeginDate,

          EndDate,

          BookingFee,

          TotalPrice,

          @Consumption.valueHelpDefinition: [{entity: {name: 'I_Currency', element: 'Currency' }}]
          CurrencyCode,

          Description,

          @Consumption.valueHelpDefinition: [{entity: { name:    'ZRAPH_I_OverallStatus',
                                                        element: 'TravelStatus' } }]
          @ObjectModel.foreignKey.association: '_TravelStatus'
          OverallStatus,

          LocalCreatedBy,

          LocalCreatedAt,

          LocalLastChangedBy,

          LocalLastChangedAt,

          LastChangedAt,

//          @ObjectModel.virtualElementCalculatedBy: 'ABAP:ZRAPH_CL_DAYS_TO_FLIGHT_LISTWD'
//          //          @ObjectModel.filter.transformedBy: 'ABAP:ZRAPH_CL_DAYS_TO_FLIGHT_LISTWD'
//          //          @ObjectModel.sort.transformedBy: 'ABAP:ZRAPH_CL_DAYS_TO_FLIGHT_LISTWD'
//          @EndUserText.label: 'Next Flight in Days'
//  virtual NextFlightInDays : abap.int2,

          @ObjectModel.virtualElementCalculatedBy: 'ABAP:ZRAPH_KEY_DATE_TRAVELWD'
          //          @ObjectModel.filter.transformedBy: 'ABAP:ZRAPH_KEY_DATE_TRAVELWD'
          @EndUserText.label: 'Date'
  virtual KeyDate          : abap.dats,

          /* Associations */
          _Booking         : redirected to composition child ZRAPH_C_BookingWDTP,
          _RoomReservation : redirected to composition child ZRAPH_C_RoomReservationWDTP,

          _Agency,
          _Currency,
          _Customer,
          _TravelStatus

}
