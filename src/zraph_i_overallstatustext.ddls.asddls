@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Travel Status Text'
@Metadata.ignorePropagatedAnnotations: true

@ObjectModel: { usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
    },
    dataCategory: #TEXT,
    representativeKey: 'TravelStatus'
}

define view entity ZRAPH_I_OverallStatusText
  as select from DDCDS_CUSTOMER_DOMAIN_VALUE_T( p_domain_name: '/DMO/OVERALL_STATUS')
  association [0..1] to I_Language as _Language on $projection.Language = _Language.Language
{

      @ObjectModel.text.element: ['TravelStatusName']
  key cast ( substring( value_low, 1, 1 ) as /dmo/overall_status preserving type ) as TravelStatus,

      @Semantics.language: true
      @ObjectModel.foreignKey.association: '_Language'
  key language                                                                     as Language,
      @Semantics.text: true
      text                                                                         as TravelStatusName,
      _Language
}
