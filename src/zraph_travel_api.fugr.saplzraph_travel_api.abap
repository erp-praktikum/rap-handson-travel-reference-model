*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZRAPH_TRAVEL_APITOP.              " Global Declarations
  INCLUDE LZRAPH_TRAVEL_APIUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZRAPH_TRAVEL_APIF...              " Subroutines
* INCLUDE LZRAPH_TRAVEL_APIO...              " PBO-Modules
* INCLUDE LZRAPH_TRAVEL_APII...              " PAI-Modules
* INCLUDE LZRAPH_TRAVEL_APIE...              " Events
* INCLUDE LZRAPH_TRAVEL_APIP...              " Local class implement.
* INCLUDE LZRAPH_TRAVEL_APIT99.              " ABAP Unit tests
