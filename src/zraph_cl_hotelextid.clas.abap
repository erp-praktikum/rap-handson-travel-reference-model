CLASS zraph_cl_hotelextid DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES: if_rap_query_provider.

  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZRAPH_CL_HOTELEXTID IMPLEMENTATION.


  METHOD if_rap_query_provider~select.

    "i've got an error because i don't call this method.
    "this error appears in free tier system, on BTP trial account it was working without them
    DATA(lv_top)     = io_request->get_paging( )->get_page_size( ).
    DATA(lv_skip)    = io_request->get_paging( )->get_offset( ).
    DATA(lt_fields)  = io_request->get_requested_elements( ).
    DATA(lt_sort)    = io_request->get_sort_elements( ).

    DATA lt_hotelheader TYPE zraph_t_hotelextid.

    IF io_request->is_data_requested( ).

      CALL FUNCTION 'Z_RAPH_HOTELEXTID_API'
        IMPORTING
          et_hotelheader = lt_hotelheader.

      DATA(lv_dyn_clause) =  io_request->get_filter( )->get_as_sql_string( ).

      SELECT * FROM @lt_hotelheader AS products
             WHERE (lv_dyn_clause)
             INTO TABLE @DATA(lt_result).

      io_response->set_data( lt_result ).

      IF io_request->is_total_numb_of_rec_requested( ).
        io_response->set_total_number_of_records( lines( lt_result ) ).
      ENDIF.

    ENDIF.

  ENDMETHOD.
ENDCLASS.
