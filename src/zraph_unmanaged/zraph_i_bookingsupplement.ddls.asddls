@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Booking Supplement Interface'
define view entity ZRAPH_I_BOOKINGSUPPLEMENT
  as select from /dmo/book_suppl as BookingSupplement
{
  key BookingSupplement.travel_id             as TravelID,
  
  key BookingSupplement.booking_id            as BookingID,
  
  key BookingSupplement.booking_supplement_id as BookingSupplementID,
  
      BookingSupplement.supplement_id         as SupplementID,
      
      @Semantics.amount.currencyCode: 'CurrencyCode'
      BookingSupplement.price                 as Price,
      
      BookingSupplement.currency_code         as CurrencyCode
}
