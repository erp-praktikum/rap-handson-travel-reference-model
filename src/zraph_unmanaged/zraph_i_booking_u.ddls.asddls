@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Booking Unmanaged'
@Metadata.ignorePropagatedAnnotations: false
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZRAPH_I_BOOKING_U
  as select from ZRAPH_I_BOOKING as Booking
  composition [0..*] of ZRAPH_I_BOOKINGSUPPLEMENT_U as _BookingSupplement
  association        to parent ZRAPH_I_TRAVEL_U     as _Travel     on  $projection.TravelID     = _Travel.TravelID
  association [1..1] to /DMO/I_Customer             as _Customer   on  $projection.CustomerID   = _Customer.CustomerID
  association [1..1] to /DMO/I_Carrier              as _Carrier    on  $projection.CarrierID    = _Carrier.AirlineID
  association [1..1] to /DMO/I_Connection           as _Connection on  $projection.CarrierID    = _Connection.AirlineID
                                                                   and $projection.ConnectionID = _Connection.ConnectionID
  association [1..1] to I_Currency                  as _Currency   on  $projection.CurrencyCode = _Currency.Currency

{
  key Booking.TravelID,

  key Booking.BookingID,

      Booking.BookingDate,

      Booking.CustomerID,

      Booking.CarrierID,

      Booking.ConnectionID,

      Booking.FlightDate,

      Booking.FlightPrice,

      Booking.CurrencyCode,

      case when Booking.FlightPrice <  600 then 3
           when Booking.FlightPrice >= 600 and Booking.FlightPrice < 1500 then 2
           when Booking.FlightPrice >= 1500 then 1
           else 0
      end as Criticality,

      /* Associations */
      _BookingSupplement,

      _Travel,

      _Customer,

      _Carrier,

      _Connection,

      _Currency


}
