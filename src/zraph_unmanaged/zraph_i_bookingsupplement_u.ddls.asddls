@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Booking Supplement Unmanaged'
@Metadata.ignorePropagatedAnnotations: false
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZRAPH_I_BOOKINGSUPPLEMENT_U
  as select from ZRAPH_I_BookingSupplement as BookingSupplement
  association        to parent ZRAPH_I_Booking_U as _Booking        on  $projection.BookingID    = _Booking.BookingID
                                                                    and $projection.TravelID     = _Booking.TravelID
  association [1..1] to ZRAPH_I_Travel_U         as _Travel         on  $projection.TravelID     = _Travel.TravelID
  association [1..1] to /DMO/I_Supplement        as _Product        on  $projection.SupplementID = _Product.SupplementID
  association [1..*] to /DMO/I_SupplementText    as _SupplementText on  $projection.SupplementID = _SupplementText.SupplementID
  association [1..1] to I_Currency               as _Currency       on  $projection.CurrencyCode = _Currency.Currency

{
  key BookingSupplement.TravelID,

  key BookingSupplement.BookingID,

  key BookingSupplement.BookingSupplementID,

      BookingSupplement.SupplementID,

      BookingSupplement.Price,

      BookingSupplement.CurrencyCode,

      /* Associations */
      _Booking,
      
      _Travel,
      
      _Product,
      
      _SupplementText,
      
      _Currency
}
