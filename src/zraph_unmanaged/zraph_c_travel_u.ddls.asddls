@EndUserText.label: 'Travel Consumption'
@AccessControl.authorizationCheck: #NOT_REQUIRED

@Metadata.allowExtensions: true
@Search.searchable: true

define root view entity ZRAPH_C_TRAVEL_U
  provider contract transactional_query
  as projection on ZRAPH_I_Travel_U

{
  key TravelID,

      @Consumption.valueHelpDefinition: [{ entity: { name:    '/DMO/I_Agency',
                                                       element: 'AgencyID' } }]
      @ObjectModel.text.element: ['AgencyName']
      @Search.defaultSearchElement: true
      AgencyID,

      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold : 0.7
      _Agency.Name       as AgencyName,

      @Consumption.valueHelpDefinition: [{ entity: { name:    '/DMO/I_Customer',
                                                       element: 'CustomerID'  } }]
      @ObjectModel.text.element: ['CustomerName']
      @Search.defaultSearchElement: true
      CustomerID,

      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold : 0.7
      _Customer.LastName as CustomerName,

      BeginDate,

      EndDate,

      BookingFee,

      @Consumption.valueHelpDefinition: [{entity: { name:    'I_Currency',
                                                      element: 'Currency' } }]
      CurrencyCode,

      Description,

      Status,

      LastChangedAt,

      /* Associations */
      _Agency,

      _Booking : redirected to composition child ZRAPH_C_Booking_U,

      _Currency,

      _Customer,

      _RoomReservation : redirected to composition child ZRAPH_C_RoomReservation_U
}
