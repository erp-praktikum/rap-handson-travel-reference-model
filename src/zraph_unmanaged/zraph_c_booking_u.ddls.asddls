@EndUserText.label: 'Booking Consumption'
@AccessControl.authorizationCheck: #NOT_REQUIRED

@Metadata.allowExtensions: true
@Search.searchable: true

define view entity ZRAPH_C_BOOKING_U
  as projection on ZRAPH_I_Booking_U
{
@Search.defaultSearchElement: true
  key TravelID,
  
  @Search.defaultSearchElement: true
  key BookingID,
  
      BookingDate,
      
      @Consumption.valueHelpDefinition: [ { entity: { name:   '/DMO/I_Customer',
                                                     element: 'CustomerID' } } ]
      @Search.defaultSearchElement: true
      @ObjectModel.text.element: ['CustomerName']
      CustomerID,
      
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold : 0.7 
      _Customer.LastName as CustomerName,
      
      @Consumption.valueHelpDefinition: [ { entity: { name:    '/DMO/I_Carrier',
                                                      element: 'AirlineID' } } ]
      @ObjectModel.text.element: ['CarrierName']
      CarrierID,
      
      _Carrier.Name      as CarrierName,
      
      @Consumption.valueHelpDefinition: [ { entity: { name:    '/DMO/I_Flight',
                                                      element: 'ConnectionID' } } ]
      ConnectionID,
      
      @Consumption.valueHelpDefinition: [ { entity: { name:    '/DMO/I_Flight',
                                                      element: 'FlightDate' } } ]
      
      FlightDate,
      
      FlightPrice,
      
      @Consumption.valueHelpDefinition: [ {entity: { name:    'I_Currency',
                                                     element: 'Currency' } } ]
      CurrencyCode,
      
      Criticality,
      
      /* Associations */
      _BookingSupplement : redirected to composition child ZRAPH_C_BookingSupplement_U,
      
      _Carrier,
      
      _Connection,
      
      _Currency,
      
      _Customer,
      
      _Travel : redirected to parent ZRAPH_C_Travel_U
}
