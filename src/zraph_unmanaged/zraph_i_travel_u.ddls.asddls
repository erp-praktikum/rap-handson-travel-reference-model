@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Travel Unmanaged'
define root view entity ZRAPH_I_TRAVEL_U
  as select from ZRAPH_I_TRAVEL as Travel
  composition [0..*] of ZRAPH_I_BOOKING_U         as _Booking
  composition [0..*] of ZRAPH_I_ROOMRESERVATION_U as _RoomReservation
  association [1..1] to /DMO/I_Agency             as _Agency   on $projection.AgencyID = _Agency.AgencyID
  association [1..1] to /DMO/I_Customer           as _Customer on $projection.CustomerID = _Customer.CustomerID
  association [1..1] to I_Currency                as _Currency on $projection.CurrencyCode = _Currency.Currency

{
  key Travel.TravelID,
  
      Travel.AgencyID,
      
      Travel.CustomerID,
      
      Travel.BeginDate,
      
      Travel.EndDate,
      
      Travel.BookingFee,
      
      Travel.CurrencyCode,
      
      Travel.Description,
      
      Travel.Status,
      
      Travel.CreatedBy,
      
      Travel.CreatedAt,
      
      Travel.LastChangedBy,
      
      Travel.LastChangedAt,

      /* Associations */
      _Booking,
      
      _RoomReservation,
      
      _Agency,
      
      _Customer,
      
      _Currency
}
