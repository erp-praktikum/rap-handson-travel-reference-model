CLASS zraph_cl_travel_auxiliary DEFINITION INHERITING FROM cl_abap_behv PUBLIC FINAL CREATE PUBLIC.
  PUBLIC SECTION.
    TYPES travel_failed              TYPE TABLE FOR FAILED   zraph_i_travel_u.
    TYPES travel_mapped              TYPE TABLE FOR MAPPED   zraph_i_travel_u.
    TYPES travel_reported            TYPE TABLE FOR REPORTED zraph_i_travel_u.
    TYPES booking_failed     TYPE TABLE FOR FAILED    zraph_i_booking_u.
    TYPES booking_mapped     TYPE TABLE FOR MAPPED    zraph_i_booking_u.
    TYPES booking_reported   TYPE TABLE FOR REPORTED  zraph_i_booking_u.

    CLASS-METHODS handle_travel_messages
      IMPORTING
        cid       TYPE string   OPTIONAL
        travel_id TYPE /dmo/travel_id OPTIONAL
        messages  TYPE /dmo/t_message
      CHANGING
        failed    TYPE travel_failed
        reported  TYPE travel_reported.

    CLASS-METHODS handle_booking_messages
      IMPORTING
        cid        TYPE string OPTIONAL
        travel_id  TYPE /dmo/travel_id OPTIONAL
        booking_id TYPE /dmo/booking_id OPTIONAL
        messages   TYPE /dmo/t_message
      CHANGING
        failed     TYPE booking_failed
        reported   TYPE booking_reported.

  PRIVATE SECTION.
    CLASS-DATA obj TYPE REF TO zraph_cl_travel_auxiliary.

    CLASS-METHODS get_message_object
      RETURNING VALUE(r_result) TYPE REF TO zraph_cl_travel_auxiliary.
ENDCLASS.



CLASS ZRAPH_CL_TRAVEL_AUXILIARY IMPLEMENTATION.


  METHOD handle_travel_messages.

    LOOP AT messages INTO DATA(message) WHERE msgty = 'E' OR msgty = 'A'.
      APPEND VALUE #( %cid     = cid
                      travelid = travel_id )
             TO failed.

      APPEND VALUE #( %msg          = get_message_object( )->new_message(
                                        id       = message-msgid
                                        number   = message-msgno
                                        severity = if_abap_behv_message=>severity-error
                                        v1       = message-msgv1
                                        v2       = message-msgv2
                                        v3       = message-msgv3
                                        v4       = message-msgv4 )
                      %key-TravelID = travel_id
                      %cid          = cid
                      TravelID      = travel_id )
             TO reported.
    ENDLOOP.
  ENDMETHOD.


  METHOD handle_booking_messages.

    LOOP AT messages INTO DATA(message) WHERE msgty = 'E' OR msgty = 'A'.
      APPEND VALUE #( %cid      = cid
                      travelid  = travel_id
                      bookingid = booking_id ) TO failed.

      APPEND VALUE #( %msg = get_message_object( )->new_message(
                                          id       = message-msgid
                                          number   = message-msgno
                                          severity = if_abap_behv_message=>severity-error
                                          v1       = message-msgv1
                                          v2       = message-msgv2
                                          v3       = message-msgv3
                                          v4       = message-msgv4 )
                      %key-TravelID = travel_id
                      %cid          = cid
                      TravelID      = travel_id
                      BookingID     = booking_id ) TO reported.

    ENDLOOP.

  ENDMETHOD.


  METHOD get_message_object.

    IF obj IS INITIAL.
      CREATE OBJECT obj.
    ENDIF.
    r_result = obj.

  ENDMETHOD.
ENDCLASS.
