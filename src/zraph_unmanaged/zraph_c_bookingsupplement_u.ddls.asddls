@EndUserText.label: 'Booking Supplement Consumption'
@AccessControl.authorizationCheck: #NOT_REQUIRED

@Metadata.allowExtensions: true
@Search.searchable: true

define view entity ZRAPH_C_BOOKINGSUPPLEMENT_U
  as projection on ZRAPH_I_BookingSupplement_U
{
      @Search.defaultSearchElement: true
  key TravelID,

      @Search.defaultSearchElement: true
  key BookingID,

      @Search.defaultSearchElement: true
  key BookingSupplementID,

      @Consumption.valueHelpDefinition: [ {entity: { name:    '/DMO/I_SUPPLEMENT',
                                                       element: 'SupplementID' },
                                             additionalBinding: [ { localElement: 'Price',        element: 'Price'},
                                                                  { localElement: 'CurrencyCode', element: 'CurrencyCode'}]}]
      @ObjectModel.text.element: ['SupplementText']

      SupplementID,

      _SupplementText.Description as SupplementText : localized,

      Price,

      @Consumption.valueHelpDefinition: [ { entity: { name:    'I_Currency',
                                                        element: 'Currency' } } ]
      CurrencyCode,

      /* Associations */
      _Booking : redirected to parent ZRAPH_C_Booking_U,
      
      _Travel : redirected to ZRAPH_C_Travel_U,      

      _Currency,

      _Product,

      _SupplementText
}
