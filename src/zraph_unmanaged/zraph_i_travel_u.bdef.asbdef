unmanaged implementation in class ZRAPH_BP_Travel_U unique;
strict ( 2 );

define behavior for ZRAPH_I_Travel_U alias Travel
implementation in class ZRAPH_BP_TRAVEL_U unique
authorization master ( global )
lock master
etag master LastChangedAt
{
  create;
  update;
  delete;

  field ( readonly ) TravelID;
  field ( mandatory ) AgencyID, CustomerID, BeginDate, EndDate;

  action set_status_booked result [1] $self;

  association _Booking { create; }
  association _RoomReservation { create; }

  mapping for /dmo/travel control /dmo/s_travel_intx
  {
    AgencyID = agency_id;
    BeginDate = begin_date;
    BookingFee = booking_fee;
    CurrencyCode = currency_code;
    CustomerID = customer_id;
    EndDate = end_date;
    LastChangedAt = lastchangedat;
    Description = description;
    Status = status;
    TravelID = travel_id;
  }
}

define behavior for ZRAPH_I_Booking_U alias Booking
implementation in class ZRAPH_BP_Booking_U unique
authorization dependent by _Travel
lock dependent by _Travel
etag dependent by _Travel
{
  update;
  delete;

  field ( readonly ) TravelID, BookingID;
  field ( mandatory ) BookingDate, CustomerID, CarrierID, ConnectionID, FlightDate;

  association _Travel;
  association _BookingSupplement { create; }

  mapping for /dmo/booking control /dmo/s_booking_intx
  {
    CarrierID = carrier_id;
    BookingDate = booking_date;
    BookingID = booking_id;
    ConnectionID = connection_id;
    CurrencyCode = currency_code;
    CustomerID = customer_id;
    FlightDate = flight_date;
    FlightPrice = flight_price;
    TravelID = travel_id;
  }
}

define behavior for ZRAPH_I_BookingSupplement_U alias BookingSupplement
implementation in class ZRAPH_BP_BookingSupplement_U unique
authorization dependent by _Travel
lock dependent by _Travel
etag dependent by _Travel
{
  update;
  delete;

  field ( readonly ) TravelID, BookingID, BookingSupplementID;
  field ( mandatory ) SupplementID, Price;

  association _Booking;
  association _Travel;

  mapping for /dmo/book_suppl control /dmo/s_booking_supplement_intx
  {
    TravelID = travel_id;
    BookingID = booking_id;
    BookingSupplementID = booking_supplement_id;
    SupplementID = supplement_id;
    Price = price;
    CurrencyCode = currency_code;
  }
}

define behavior for ZRAPH_I_RoomReservation_U alias RoomReservation
implementation in class ZRAPH_BP_RoomReservation_U unique
authorization dependent by _Travel
lock dependent by _Travel
etag dependent by _Travel
{
  update;
  delete;

  field ( readonly ) TravelID, RoomReservationID;
  field ( mandatory ) HotelID, BeginDate, EndDate, RoomType, RoomPrice, CurrencyCode;

  association _Travel;

  mapping for zraph_roomrsv_u control zraph_s_room_reservation_intx
  {
    TravelID = travel_id;
    RoomReservationID = roomrsv_id;
    HotelID = hotel_id;
    BeginDate = begin_date;
    EndDate = end_date;
    RoomType = room_type;
    RoomPrice = roomrsv_price;
    CurrencyCode = currency_code;
  }
}