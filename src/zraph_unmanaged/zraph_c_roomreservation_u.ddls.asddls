@EndUserText.label: 'Room Reservation Consumption'
@AccessControl.authorizationCheck: #NOT_REQUIRED

@Metadata.allowExtensions: true
@Search.searchable: true

define view entity ZRAPH_C_ROOMRESERVATION_U
  as projection on ZRAPH_I_RoomReservation_U
{
      @Search.defaultSearchElement: true
  key TravelID,

      @Search.defaultSearchElement: true
  key RoomReservationID,

      @Search.defaultSearchElement: true
      @Consumption.valueHelpDefinition: [{ entity: { name:    'ZRAPH_I_Hotel',
                                                     element: 'HotelID'  } }]
      HotelID,

      BeginDate,

      EndDate,

      @Consumption.valueHelpDefinition: [{ entity: { name:    'ZRAPH_I_HotelRoomType',
                                                     element: 'Value'  } }]
      @ObjectModel.text.element: ['RoomTypeText']
      @Search.defaultSearchElement: true
      RoomType,

      _HotelRoomType.Text as RoomTypeText,

      RoomPrice,

      @Consumption.valueHelpDefinition: [{entity: { name:    'I_Currency',
                                                    element: 'Currency' } }]
      CurrencyCode,

      @Search.defaultSearchElement: true
      _Hotel.Name         as HotelName,

      @Search.defaultSearchElement: true
      _Hotel.City         as HotelCity,

      /* Associations */
      _Currency,

      _Hotel,

      _HotelRoomType,

      _Travel : redirected to parent ZRAPH_C_Travel_U
}
