@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Room Reservation Unmanaged'
@Metadata.ignorePropagatedAnnotations: false
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZRAPH_I_ROOMRESERVATION_U
  as select from ZRAPH_I_ROOMRESERVATION as RoomReservation
  association        to parent ZRAPH_I_TRAVEL_U as _Travel        on $projection.TravelID = _Travel.TravelID
  association [1..1] to ZRAPH_I_HOTEL           as _Hotel         on $projection.HotelID  = _Hotel.HotelID
  association [1..1] to ZRAPH_I_HotelRoomType   as _HotelRoomType on $projection.RoomType = _HotelRoomType.Value
  association [1..1] to I_Currency              as _Currency      on $projection.CurrencyCode = _Currency.Currency

{
  key RoomReservation.TravelID,

  key RoomReservation.RoomReservationID,

      RoomReservation.HotelID,

      RoomReservation.BeginDate,

      RoomReservation.EndDate,

      RoomReservation.RoomType,

      RoomReservation.RoomPrice,

      RoomReservation.CurrencyCode,

      /* Associations */
      _Travel,
      
      _Hotel,
      
      _HotelRoomType,
      
      _Currency
}
