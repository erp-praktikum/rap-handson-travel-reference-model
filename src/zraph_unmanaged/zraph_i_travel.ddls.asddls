@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Travel Interface'
@Metadata.ignorePropagatedAnnotations: true
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZRAPH_I_TRAVEL
  as select from /dmo/travel as Travel
{
  key Travel.travel_id     as TravelID,

      Travel.agency_id     as AgencyID,

      Travel.customer_id   as CustomerID,

      Travel.begin_date    as BeginDate,

      Travel.end_date      as EndDate,

      @Semantics.amount.currencyCode: 'CurrencyCode'
      Travel.booking_fee   as BookingFee,

      Travel.currency_code as CurrencyCode,

      Travel.description   as Description,

      Travel.status        as Status,

      Travel.createdby     as CreatedBy,

      Travel.createdat     as CreatedAt,

      Travel.lastchangedby as LastChangedBy,

      Travel.lastchangedat as LastChangedAt
}
