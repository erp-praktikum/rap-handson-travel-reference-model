@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Room Reservation Interface'
define view entity ZRAPH_I_ROOMRESERVATION
  as select from zraph_roomrsv_u as RoomReservation
{
  key RoomReservation.travel_id     as TravelID,
  
  key RoomReservation.roomrsv_id    as RoomReservationID,
  
      RoomReservation.hotel_id      as HotelID,
      
      RoomReservation.begin_date    as BeginDate,
      
      RoomReservation.end_date      as EndDate,
      
      RoomReservation.room_type     as RoomType,
      
      @Semantics.amount.currencyCode: 'CurrencyCode'
      RoomReservation.roomrsv_price as RoomPrice,
      
      RoomReservation.currency_code as CurrencyCode
}
