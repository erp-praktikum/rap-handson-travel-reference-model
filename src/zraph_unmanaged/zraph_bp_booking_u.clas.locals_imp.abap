CLASS lhc_Booking DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.

    METHODS update FOR MODIFY
      IMPORTING entities FOR UPDATE Booking.

    METHODS delete FOR MODIFY
      IMPORTING keys FOR DELETE Booking.

    METHODS read FOR READ
      IMPORTING keys FOR READ Booking RESULT result.

    METHODS rba_Bookingsupplement FOR READ
      IMPORTING keys_rba FOR READ Booking\_Bookingsupplement FULL result_requested RESULT result LINK association_links.

    METHODS rba_Travel FOR READ
      IMPORTING keys_rba FOR READ Booking\_Travel FULL result_requested RESULT result LINK association_links.

    METHODS cba_Bookingsupplement FOR MODIFY
      IMPORTING entities_cba FOR CREATE Booking\_Bookingsupplement.

ENDCLASS.

CLASS lhc_Booking IMPLEMENTATION.

  METHOD update.

    DATA messages TYPE /dmo/t_message.
    DATA booking  TYPE /dmo/booking.
    DATA bookingx TYPE /dmo/s_booking_inx.

    LOOP AT entities ASSIGNING FIELD-SYMBOL(<booking_update>).

      booking = CORRESPONDING #( <booking_update> MAPPING FROM ENTITY ).

      bookingx-booking_id = <booking_update>-BookingID.
      bookingx-_intx      = CORRESPONDING #( <booking_update> MAPPING FROM ENTITY ).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_UPDATE'
        EXPORTING
          is_travel   = VALUE /dmo/s_travel_in( travel_id = <booking_update>-travelid )
          is_travelx  = VALUE /dmo/s_travel_inx( travel_id = <booking_update>-travelid )
          it_booking  = VALUE /dmo/t_booking_in( ( CORRESPONDING #( booking ) ) )
          it_bookingx = VALUE /dmo/t_booking_inx( ( bookingx ) )
        IMPORTING
          et_messages = messages.


      zraph_cl_travel_auxiliary=>handle_booking_messages(
        EXPORTING
          cid        = <booking_update>-%cid_ref
          travel_id  = <booking_update>-travelid
          booking_id = <booking_update>-bookingid
          messages   = messages
        CHANGING
          failed   = failed-booking
          reported = reported-booking ).

    ENDLOOP.

  ENDMETHOD.

  METHOD delete.

    DATA messages TYPE /dmo/t_message.

    LOOP AT keys INTO DATA(booking_delete).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_UPDATE'
        EXPORTING
          is_travel   = VALUE /dmo/s_travel_in( travel_id = booking_delete-travelid )
          is_travelx  = VALUE /dmo/s_travel_inx( travel_id = booking_delete-travelid )
          it_booking  = VALUE /dmo/t_booking_in( ( booking_id = booking_delete-bookingid ) )
          it_bookingx = VALUE /dmo/t_booking_inx( ( booking_id  = booking_delete-bookingid
                                                    action_code = /dmo/if_flight_legacy=>action_code-delete ) )
        IMPORTING
          et_messages = messages.

      IF messages IS NOT INITIAL.

        zraph_cl_travel_auxiliary=>handle_booking_messages(
         EXPORTING
           cid        = booking_delete-%cid_ref
           travel_id  = booking_delete-travelid
           booking_id = booking_delete-bookingid
           messages   = messages
         CHANGING
           failed   = failed-booking
           reported = reported-booking ).

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD read.

    DATA: travel_out  TYPE /dmo/travel,
          booking_out TYPE /dmo/t_booking,
          messages     TYPE /dmo/t_message.

    "Only one function call for each requested travelid
    LOOP AT keys ASSIGNING FIELD-SYMBOL(<travel_read>)
                            GROUP BY <travel_read>-travelid .

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_READ'
        EXPORTING
          iv_travel_id = <travel_read>-travelid
        IMPORTING
          es_travel    = travel_out
          et_booking   = booking_out
          et_messages  = messages.

      IF messages IS INITIAL.
        "For each travelID find the requested bookings
        LOOP AT GROUP <travel_read> ASSIGNING FIELD-SYMBOL(<booking_read>).

          READ TABLE booking_out INTO DATA(booking) WITH KEY travel_id  = <booking_read>-%key-TravelID
                                                             booking_id = <booking_read>-%key-BookingID .
          "if read was successful
          IF sy-subrc = 0.

            "fill result parameter with flagged fields
            INSERT
              VALUE #( travelid      =   booking-travel_id
                       bookingid     =   booking-booking_id
                       bookingdate   =   COND #( WHEN <booking_read>-%control-BookingDate      = cl_abap_behv=>flag_changed THEN booking-booking_date   )
                       customerid    =   COND #( WHEN <booking_read>-%control-CustomerID       = cl_abap_behv=>flag_changed THEN booking-customer_id    )
                       carrierid     =   COND #( WHEN <booking_read>-%control-CarrierID        = cl_abap_behv=>flag_changed THEN booking-carrier_id     )
                       connectionid  =   COND #( WHEN <booking_read>-%control-ConnectionID     = cl_abap_behv=>flag_changed THEN booking-connection_id  )
                       flightdate    =   COND #( WHEN <booking_read>-%control-FlightDate       = cl_abap_behv=>flag_changed THEN booking-flight_date    )
                       flightprice   =   COND #( WHEN <booking_read>-%control-FlightPrice      = cl_abap_behv=>flag_changed THEN booking-flight_price   )
                       currencycode  =   COND #( WHEN <booking_read>-%control-CurrencyCode     = cl_abap_behv=>flag_changed THEN booking-currency_code  )
                     ) INTO TABLE result.
          ELSE.
            "BookingID not found
            INSERT
              VALUE #( travelid    = <booking_read>-TravelID
                       bookingid   = <booking_read>-BookingID
                       %fail-cause = if_abap_behv=>cause-not_found )
              INTO TABLE failed-booking.
          ENDIF.
        ENDLOOP.
      ELSE.
        "TravelID not found or other fail cause
        LOOP AT GROUP <travel_read> ASSIGNING <booking_read>.
          failed-booking = VALUE #(  BASE failed-booking
                                     FOR msg IN messages ( %key-TravelID    = <booking_read>-TravelID
                                                           %key-BookingID   = <booking_read>-BookingID
                                                           %fail-cause      = COND #( WHEN msg-msgty = 'E' AND msg-msgno = '016'
                                                                                      THEN if_abap_behv=>cause-not_found
                                                                                      ELSE if_abap_behv=>cause-unspecific ) ) ).
        ENDLOOP.

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD rba_Bookingsupplement.
  ENDMETHOD.

  METHOD rba_Travel.
  ENDMETHOD.

  METHOD cba_Bookingsupplement.
  ENDMETHOD.

ENDCLASS.
