CLASS lhc_Travel DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.

    METHODS create_travel FOR MODIFY
      IMPORTING entities FOR CREATE Travel.

    METHODS update_travel FOR MODIFY
      IMPORTING entities FOR UPDATE Travel.

    METHODS delete_travel FOR MODIFY
      IMPORTING keys FOR DELETE Travel.

    METHODS read_travel FOR READ
      IMPORTING keys FOR READ Travel RESULT result.

    METHODS rba_booking FOR READ
      IMPORTING keys_rba FOR READ Travel\_Booking FULL result_requested RESULT result LINK association_links.

    METHODS rba_roomreservation FOR READ
      IMPORTING keys_rba FOR READ Travel\_Roomreservation FULL result_requested RESULT result LINK association_links.

    METHODS cba_booking FOR MODIFY
      IMPORTING entities_cba FOR CREATE Travel\_Booking.

    METHODS cba_roomreservation FOR MODIFY
      IMPORTING entities_cba FOR CREATE Travel\_Roomreservation.

    METHODS set_status_booked FOR MODIFY
      IMPORTING keys FOR ACTION Travel~set_status_booked RESULT result.

    METHODS get_global_authorizations FOR GLOBAL AUTHORIZATION
      IMPORTING REQUEST requested_authorizations FOR Travel RESULT result.

    METHODS lock FOR LOCK
      IMPORTING keys FOR LOCK Travel.

ENDCLASS.

CLASS lhc_Travel IMPLEMENTATION.

  METHOD create_travel.

    DATA messages   TYPE /dmo/t_message.
    DATA travel_in  TYPE /dmo/travel.
    DATA travel_out TYPE /dmo/travel.

    LOOP AT entities ASSIGNING FIELD-SYMBOL(<travel>).
      CLEAR travel_in.
      travel_in = CORRESPONDING #( <travel> MAPPING FROM ENTITY USING CONTROL ).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_CREATE'
        EXPORTING
          is_travel   = travel_in
        IMPORTING
          es_travel   = travel_out
          et_messages = messages.

      IF messages IS INITIAL.
        INSERT VALUE #( %cid = <travel>-%cid  travelid = travel_out-travel_id )
                         INTO TABLE mapped-travel.
      ELSE.
        zraph_cl_travel_auxiliary=>handle_travel_messages(
            EXPORTING
              cid       = <travel>-%cid
              messages  = messages
            CHANGING
              failed    = failed-travel
              reported  = reported-travel
          ).
      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD update_travel.

    DATA messages TYPE /dmo/t_message.
    DATA travel   TYPE /dmo/travel.
    DATA travelx  TYPE /dmo/s_travel_inx. "refers to x structure (> BAPIs)

    LOOP AT entities ASSIGNING FIELD-SYMBOL(<travel>).

      travel = CORRESPONDING #( <travel> MAPPING FROM ENTITY ).

      travelx-travel_id = <travel>-TravelID.
      travelx-_intx     = CORRESPONDING #( <travel> MAPPING FROM ENTITY ).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_UPDATE'
        EXPORTING
          is_travel   = CORRESPONDING /dmo/s_travel_in( travel )
          is_travelx  = travelx
        IMPORTING
          et_messages = messages.

      zraph_cl_travel_auxiliary=>handle_travel_messages(
        EXPORTING
          cid       = <travel>-%cid_ref
          travel_id = <travel>-travelid
          messages  = messages
        CHANGING
          failed   = failed-travel
          reported = reported-travel
      ).

    ENDLOOP.

  ENDMETHOD.

  METHOD delete_travel.

    DATA messages TYPE /dmo/t_message.

    LOOP AT keys ASSIGNING FIELD-SYMBOL(<travel_delete>).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_DELETE'
        EXPORTING
          iv_travel_id = <travel_delete>-travelid
        IMPORTING
          et_messages  = messages.

      zraph_cl_travel_auxiliary=>handle_travel_messages(
        EXPORTING
          cid       = <travel_delete>-%cid_ref
          travel_id = <travel_delete>-travelid
          messages  = messages
        CHANGING
          failed       = failed-travel
          reported     = reported-travel
      ).

    ENDLOOP.

  ENDMETHOD.

  METHOD read_travel.
    DATA: travel_out TYPE /dmo/travel,
          messages   TYPE /dmo/t_message.

    LOOP AT keys INTO DATA(travel_to_read).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_READ'
        EXPORTING
          iv_travel_id = travel_to_read-travelid
        IMPORTING
          es_travel    = travel_out
          et_messages  = messages.

      IF messages IS INITIAL.
        "fill result parameter with flagged fields

        INSERT CORRESPONDING #( travel_out MAPPING TO ENTITY ) INTO TABLE result.

      ELSE.
        "fill failed table in case of error

        failed-travel = VALUE #(
          BASE failed-travel
          FOR msg IN messages (
            %key = travel_to_read-%key
            %fail-cause = COND #(
              WHEN msg-msgty = 'E' AND msg-msgno = '016'
              THEN if_abap_behv=>cause-not_found
              ELSE if_abap_behv=>cause-unspecific
            )
          )
        ).

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD rba_booking.
    DATA: travel_out   TYPE /dmo/travel,
          bookings_out TYPE /dmo/t_booking,
          booking      LIKE LINE OF result,
          messages     TYPE /dmo/t_message.

    LOOP AT keys_rba ASSIGNING FIELD-SYMBOL(<travel_rba>).

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_READ'
        EXPORTING
          iv_travel_id = <travel_rba>-travelid
        IMPORTING
          es_travel    = travel_out
          et_booking   = bookings_out
          et_messages  = messages.

      IF messages IS INITIAL.

        LOOP AT bookings_out ASSIGNING FIELD-SYMBOL(<booking>).
          "fill link table with key fields

          INSERT
            VALUE #(
              source-%key = <travel_rba>-%key
              target-%key = VALUE #(
                TravelID  = <booking>-travel_id
                BookingID = <booking>-booking_id
              )
            )
            INTO TABLE association_links.

          "fill result parameter with flagged fields
          IF result_requested = abap_true.

            booking = CORRESPONDING #( <booking> MAPPING TO ENTITY ).
            INSERT booking INTO TABLE result.

          ENDIF.

        ENDLOOP.

      ELSE.
        "fill failed table in case of error

        failed-travel = VALUE #(
          BASE failed-travel
          FOR msg IN messages (
            %key = <travel_rba>-TravelID
            %fail-cause = COND #(
              WHEN msg-msgty = 'E' AND msg-msgno = '016'
              THEN if_abap_behv=>cause-not_found
              ELSE if_abap_behv=>cause-unspecific
            )
          )
        ).

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD rba_roomreservation.
  ENDMETHOD.

  METHOD cba_booking.
    DATA messages        TYPE /dmo/t_message.
    DATA bookings_old     TYPE /dmo/t_booking.
    DATA booking         TYPE /dmo/booking.
    DATA last_booking_id TYPE /dmo/booking_id VALUE '0'.

    LOOP AT entities_cba ASSIGNING FIELD-SYMBOL(<booking_create_ba>).

      DATA(travelid) = <booking_create_ba>-travelid.

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_READ'
        EXPORTING
          iv_travel_id = travelid
        IMPORTING
          et_booking   = bookings_old
          et_messages  = messages.

      IF messages IS INITIAL.

        IF bookings_old IS NOT INITIAL.

          last_booking_id = bookings_old[ lines( bookings_old ) ]-booking_id.

        ENDIF.

        LOOP AT <booking_create_ba>-%target ASSIGNING FIELD-SYMBOL(<booking_create>).

          booking = CORRESPONDING #( <booking_create> MAPPING FROM ENTITY USING CONTROL ) .

          last_booking_id += 1.
          booking-booking_id = last_booking_id.

          CALL FUNCTION '/DMO/FLIGHT_TRAVEL_UPDATE'
            EXPORTING
              is_travel   = VALUE /dmo/s_travel_in( travel_id = travelid )
              is_travelx  = VALUE /dmo/s_travel_inx( travel_id = travelid )
              it_booking  = VALUE /dmo/t_booking_in( ( CORRESPONDING #( booking ) ) )
              it_bookingx = VALUE /dmo/t_booking_inx(
                (
                  booking_id  = booking-booking_id
                  action_code = /dmo/if_flight_legacy=>action_code-create
                )
              )
            IMPORTING
              et_messages = messages.

          IF messages IS INITIAL.

            INSERT
              VALUE #(
                %cid = <booking_create>-%cid
                travelid = travelid
                bookingid = booking-booking_id
              )
              INTO TABLE mapped-booking.

          ELSE.

            LOOP AT messages INTO DATA(ls_message) WHERE msgty = 'E' OR msgty = 'A'.

              INSERT VALUE #( %cid = <booking_create>-%cid ) INTO TABLE failed-booking.

              INSERT
                VALUE #(
                  %cid     = <booking_create>-%cid
                  travelid = <booking_create>-TravelID
                  %msg     = new_message(
                    id       = ls_message-msgid
                    number   = ls_message-msgno
                    severity = if_abap_behv_message=>severity-error
                    v1       = ls_message-msgv1
                    v2       = ls_message-msgv2
                    v3       = ls_message-msgv3
                    v4       = ls_message-msgv4
                  )
                )
                INTO TABLE reported-booking.

            ENDLOOP.

          ENDIF.

        ENDLOOP.

      ELSE.

        zraph_cl_travel_auxiliary=>handle_travel_messages(
          EXPORTING
            cid       = <booking_create_ba>-%cid_ref
            travel_id = travelid
            messages  = messages
          CHANGING
            failed       = failed-travel
            reported     = reported-travel
        ).

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD cba_roomreservation.
  ENDMETHOD.

  METHOD set_status_booked.

    DATA messages TYPE /dmo/t_message.
    DATA travel_out TYPE /dmo/travel.
    DATA travel_set_status_booked LIKE LINE OF result.

    CLEAR travel_set_status_booked.

    LOOP AT keys ASSIGNING FIELD-SYMBOL(<travel_set_status_booked>).

      DATA(travelid) = <travel_set_status_booked>-travelid.

      CALL FUNCTION '/DMO/FLIGHT_TRAVEL_SET_BOOKING'
        EXPORTING
          iv_travel_id = travelid
        IMPORTING
          et_messages  = messages.

      IF messages IS INITIAL.

        CALL FUNCTION '/DMO/FLIGHT_TRAVEL_READ'
          EXPORTING
            iv_travel_id = travelid
          IMPORTING
            es_travel    = travel_out.

        travel_set_status_booked-travelid        = travelid.
        travel_set_status_booked-%param          = CORRESPONDING #( travel_out MAPPING TO ENTITY ).
        travel_set_status_booked-%param-travelid = travelid.
        APPEND travel_set_status_booked TO result.

      ELSE.

        zraph_cl_travel_auxiliary=>handle_travel_messages(
          EXPORTING
            cid       = <travel_set_status_booked>-%cid_ref
            travel_id = travelid
            messages  = messages
          CHANGING
            failed       = failed-travel
            reported     = reported-travel
        ).

      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD get_global_authorizations.
    result-%create = if_abap_behv=>auth-allowed.
    result-%update =  if_abap_behv=>auth-allowed.
    result-%action-set_status_booked =  if_abap_behv=>auth-allowed.
    result-%delete = if_abap_behv=>auth-allowed.
  ENDMETHOD.

  METHOD lock.
  ENDMETHOD.

ENDCLASS.

CLASS lsc_zraph_I_TRAVEL_U DEFINITION INHERITING FROM cl_abap_behavior_saver.
  PROTECTED SECTION.

    METHODS finalize REDEFINITION.

    METHODS check_before_save REDEFINITION.

    METHODS save REDEFINITION.

    METHODS cleanup REDEFINITION.

    METHODS cleanup_finalize REDEFINITION.

ENDCLASS.

CLASS lsc_zraph_I_TRAVEL_U IMPLEMENTATION.

  METHOD finalize.
  ENDMETHOD.

  METHOD check_before_save.
  ENDMETHOD.

  METHOD save.
    CALL FUNCTION '/DMO/FLIGHT_TRAVEL_SAVE'.
  ENDMETHOD.

  METHOD cleanup.
    CALL FUNCTION '/DMO/FLIGHT_TRAVEL_INITIALIZE'.
  ENDMETHOD.

  METHOD cleanup_finalize.
  ENDMETHOD.

ENDCLASS.
