@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'RAP HandsOn: Hotel (DB)'
define view entity ZRAPH_I_HOTEL
  as select from zraph_hotel
{
  key hotel_id as HotelID,
      name     as Name,
      city     as City,
      country  as Country
}
