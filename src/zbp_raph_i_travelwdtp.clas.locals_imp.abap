CLASS lhc_travel DEFINITION INHERITING FROM cl_abap_behavior_handler.

  PRIVATE SECTION.

    CONSTANTS:
      BEGIN OF travel_status,
        open     TYPE c LENGTH 1 VALUE 'O', "Open
        accepted TYPE c LENGTH 1 VALUE 'A', "Accepted
        rejected TYPE c LENGTH 1 VALUE 'X', "Rejected
      END OF travel_status.

    METHODS validatedates FOR VALIDATE ON SAVE
      IMPORTING keys FOR travel~validatedates.
    METHODS validatecustomer FOR VALIDATE ON SAVE
      IMPORTING keys FOR travel~validatecustomer.
    METHODS validateagency FOR VALIDATE ON SAVE
      IMPORTING keys FOR travel~validateagency.
    METHODS get_features FOR FEATURES
      IMPORTING keys REQUEST requested_features FOR travel RESULT result.
    METHODS get_authorizations FOR AUTHORIZATION
      IMPORTING keys REQUEST requested_authorizations FOR travel RESULT result.
    METHODS recalctotalprice FOR MODIFY
      IMPORTING keys FOR ACTION travel~recalctotalprice.
    METHODS calculatetotalprice FOR DETERMINE ON MODIFY
      IMPORTING keys FOR travel~calculatetotalprice.
    METHODS setinitialstatus FOR DETERMINE ON MODIFY
      IMPORTING keys FOR travel~setinitialstatus.
    METHODS accepttravel FOR MODIFY
      IMPORTING keys FOR ACTION travel~accepttravel RESULT result.
    METHODS rejecttravel FOR MODIFY
      IMPORTING keys FOR ACTION travel~rejecttravel RESULT result.
    METHODS settravelid FOR DETERMINE ON MODIFY
      IMPORTING keys FOR travel~settravelid.
    METHODS is_update_granted
      IMPORTING has_before_image      TYPE abap_bool
                overall_status        TYPE /dmo/overall_status
      RETURNING VALUE(update_granted) TYPE abap_bool.
    METHODS is_delete_granted
      IMPORTING has_before_image      TYPE abap_bool
                overall_status        TYPE /dmo/overall_status
      RETURNING VALUE(delete_granted) TYPE abap_bool.
    METHODS is_create_granted
      RETURNING VALUE(create_granted) TYPE abap_bool.

ENDCLASS.

CLASS lhc_travel IMPLEMENTATION.

  METHOD get_features.

    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
          ENTITY travel
            FIELDS ( overallstatus )
            WITH CORRESPONDING #( keys )
          RESULT DATA(lt_travel)
          FAILED failed.


    result = VALUE #( FOR ls_travel IN lt_travel
                          ( %tky                   = ls_travel-%tky

                            %field-bookingfee      = COND #( WHEN ls_travel-overallstatus = travel_status-accepted
                                                             THEN if_abap_behv=>fc-f-read_only
                                                             ELSE if_abap_behv=>fc-f-unrestricted )
                            %action-accepttravel   = COND #( WHEN ls_travel-overallstatus = travel_status-accepted
                                                             THEN if_abap_behv=>fc-o-disabled
                                                             ELSE if_abap_behv=>fc-o-enabled )
                            %action-rejecttravel   = COND #( WHEN ls_travel-overallstatus = travel_status-rejected
                                                             THEN if_abap_behv=>fc-o-disabled
                                                             ELSE if_abap_behv=>fc-o-enabled )
                            %assoc-_booking        = COND #( WHEN ls_travel-overallstatus = travel_status-rejected
                                                            THEN if_abap_behv=>fc-o-disabled
                                                            ELSE if_abap_behv=>fc-o-enabled )
                          ) ).
  ENDMETHOD.

  METHOD get_authorizations.
    DATA: has_before_image    TYPE abap_bool,
          is_update_requested TYPE abap_bool,
          is_delete_requested TYPE abap_bool,
          update_granted      TYPE abap_bool,
          delete_granted      TYPE abap_bool.

    DATA: failed_travel LIKE LINE OF failed-travel.

    " Read the existing travels
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        FIELDS ( overallstatus ) WITH CORRESPONDING #( keys )
      RESULT DATA(lt_travels)
      FAILED failed.

    CHECK lt_travels IS NOT INITIAL.

*   In this example the authorization is defined based on the Activity + Travel Status
*   For the Travel Status we need the before-image from the database. We perform this for active (is_draft=00) as well as for drafts (is_draft=01) as we can't distinguish between edit or new drafts
    SELECT FROM /dmo/a_travel_d
      FIELDS travel_uuid, overall_status
      FOR ALL ENTRIES IN @lt_travels
      WHERE travel_uuid EQ @lt_travels-traveluuid
      ORDER BY PRIMARY KEY
      INTO TABLE @DATA(lt_travels_before_image).

    is_update_requested = COND #( WHEN requested_authorizations-%update              = if_abap_behv=>mk-on OR
                                       requested_authorizations-%action-accepttravel = if_abap_behv=>mk-on OR
                                       requested_authorizations-%action-rejecttravel = if_abap_behv=>mk-on OR
                                       requested_authorizations-%assoc-_booking      = if_abap_behv=>mk-on
                                  THEN abap_true ELSE abap_false ).

    is_delete_requested = COND #( WHEN requested_authorizations-%delete = if_abap_behv=>mk-on
                                    THEN abap_true ELSE abap_false ).

    LOOP AT lt_travels INTO DATA(ls_travel).
      update_granted = delete_granted = abap_false.

      READ TABLE lt_travels_before_image INTO DATA(ls_travel_before_image)
           WITH KEY travel_uuid = ls_travel-traveluuid BINARY SEARCH.
      has_before_image = COND #( WHEN sy-subrc = 0 THEN abap_true ELSE abap_false ).

      IF is_update_requested = abap_true.
        " Edit of an existing record -> check update authorization
        IF has_before_image = abap_true.
          update_granted = is_update_granted( has_before_image = has_before_image  overall_status = ls_travel_before_image-overall_status ).
          IF update_granted = abap_false.
            APPEND VALUE #( %tky        = ls_travel-%tky
                            %msg        = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                       number   = '011' " Not authorized
                                                       severity = if_abap_behv_message=>severity-error )
                          ) TO reported-travel.
          ENDIF.
          " Creation of a new record -> check create authorization
        ELSE.
          update_granted = is_create_granted( ).
          IF update_granted = abap_false.
            APPEND VALUE #( %tky        = ls_travel-%tky
                            %msg        = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                       number   = '011' " Not authorized
                                                       severity = if_abap_behv_message=>severity-error )
                          ) TO reported-travel.
          ENDIF.
        ENDIF.
      ENDIF.

      IF is_delete_requested = abap_true.
        delete_granted = is_delete_granted( has_before_image = has_before_image  overall_status = ls_travel_before_image-overall_status ).
        IF delete_granted = abap_false.
          APPEND VALUE #( %tky        = ls_travel-%tky
                          %msg        = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                     number   = '011' " Not authorized
                                                     severity = if_abap_behv_message=>severity-error )
                        ) TO reported-travel.
        ENDIF.
      ENDIF.

      APPEND VALUE #( %tky = ls_travel-%tky
                      %update              = COND #( WHEN update_granted = abap_true THEN if_abap_behv=>auth-allowed ELSE if_abap_behv=>auth-unauthorized )
                      %action-accepttravel = COND #( WHEN update_granted = abap_true THEN if_abap_behv=>auth-allowed ELSE if_abap_behv=>auth-unauthorized )
                      %action-rejecttravel = COND #( WHEN update_granted = abap_true THEN if_abap_behv=>auth-allowed ELSE if_abap_behv=>auth-unauthorized )
                      %assoc-_booking      = COND #( WHEN update_granted = abap_true THEN if_abap_behv=>auth-allowed ELSE if_abap_behv=>auth-unauthorized )
                      %delete              = COND #( WHEN delete_granted = abap_true THEN if_abap_behv=>auth-allowed ELSE if_abap_behv=>auth-unauthorized )
                    )
        TO result.
    ENDLOOP.
  ENDMETHOD.

  METHOD recalctotalprice.
    TYPES: BEGIN OF ty_amount_per_currencycode,
             amount        TYPE /dmo/total_price,
             currency_code TYPE /dmo/currency_code,
           END OF ty_amount_per_currencycode.

    DATA: amount_per_currencycode TYPE STANDARD TABLE OF ty_amount_per_currencycode.

    " Read all relevant travel instances.
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
         ENTITY travel
            FIELDS ( bookingfee currencycode )
            WITH CORRESPONDING #( keys )
         RESULT DATA(lt_travel)
         FAILED failed.

    DELETE lt_travel WHERE currencycode IS INITIAL.

    LOOP AT lt_travel ASSIGNING FIELD-SYMBOL(<fs_travel>).
      " Set the start for the calculation by adding the booking fee.
      amount_per_currencycode = VALUE #( ( amount        = <fs_travel>-bookingfee
                                           currency_code = <fs_travel>-currencycode ) ).

      " Read all associated room reservation and add them to the total price.
      READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
        ENTITY travel BY \_roomreservation
          FIELDS ( roomrsvprice currencycode )
        WITH VALUE #( ( %key = <fs_travel>-%key ) )
        RESULT DATA(lt_roomreservation).

      LOOP AT lt_roomreservation INTO DATA(roomreservation) WHERE currencycode IS NOT INITIAL.
        COLLECT VALUE ty_amount_per_currencycode( amount        = roomreservation-roomrsvprice
                                                  currency_code = roomreservation-currencycode ) INTO amount_per_currencycode.
      ENDLOOP.

      " Read all associated bookings and add them to the total price.
      READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
        ENTITY travel BY \_booking
          FIELDS ( flightprice currencycode )
        WITH VALUE #( ( %key = <fs_travel>-%key ) )
        RESULT DATA(lt_booking).

      LOOP AT lt_booking INTO DATA(booking) WHERE currencycode IS NOT INITIAL.
        COLLECT VALUE ty_amount_per_currencycode( amount        = booking-flightprice
                                                  currency_code = booking-currencycode ) INTO amount_per_currencycode.
      ENDLOOP.

      " Read all associated booking supplements and add them to the total price.
      READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
        ENTITY booking BY \_bookingsupplement
          FIELDS (  price currencycode )
        WITH VALUE #( FOR rba_booking IN lt_booking ( %tky = rba_booking-%tky ) )
        RESULT DATA(lt_bookingsupplement).

      LOOP AT lt_bookingsupplement INTO DATA(bookingsupplement) WHERE currencycode IS NOT INITIAL.
        COLLECT VALUE ty_amount_per_currencycode( amount        = bookingsupplement-price
                                                  currency_code = bookingsupplement-currencycode ) INTO amount_per_currencycode.
      ENDLOOP.

      CLEAR <fs_travel>-totalprice.
      LOOP AT amount_per_currencycode INTO DATA(single_amount_per_currencycode).
        " If needed do a Currency Conversion
        IF single_amount_per_currencycode-currency_code = <fs_travel>-currencycode.
          <fs_travel>-totalprice += single_amount_per_currencycode-amount.
        ELSE.
          /dmo/cl_flight_amdp=>convert_currency(
            EXPORTING
              iv_amount               = single_amount_per_currencycode-amount
              iv_currency_code_source = single_amount_per_currencycode-currency_code
              iv_currency_code_target = <fs_travel>-currencycode
              iv_exchange_rate_date   = cl_abap_context_info=>get_system_date( )
            IMPORTING
              ev_amount               = DATA(total_booking_price_per_curr)
          ).
          <fs_travel>-totalprice += total_booking_price_per_curr.
        ENDIF.
      ENDLOOP.
    ENDLOOP.

    " write back the modified total_price of travels
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        UPDATE FIELDS ( totalprice )
        WITH CORRESPONDING #( lt_travel ).
  ENDMETHOD.


  METHOD calculatetotalprice.
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        EXECUTE recalctotalprice
        FROM CORRESPONDING #( keys )
    REPORTED DATA(lt_reported).

    reported = CORRESPONDING #( DEEP lt_reported ).
  ENDMETHOD.

  METHOD validatedates.

    "Read relevant travel instance data
    "Long version with 'entities'
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        FIELDS ( travelid begindate enddate ) WITH CORRESPONDING #( keys )
      RESULT DATA(lt_travel)
      FAILED DATA(lt_failed).

*    "SHORT VERSION WITH 'ENTITY'
*    READ ENTITY ZRAPH_I_TravelWDTP "Fields (  )
*    FROM CORRESPONDING #( keys )
*    RESULT DATA(lt_travel).

    failed = CORRESPONDING #( DEEP lt_failed ).

    LOOP AT lt_travel INTO DATA(ls_travel).

      APPEND VALUE #(  %tky                = ls_travel-%tky
                       %state_area         = 'VALIDATE_DATES' ) TO reported-travel.

      IF ls_travel-begindate IS INITIAL.
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.

        APPEND VALUE #( %tky               = ls_travel-%tky
                        %state_area        = 'VALIDATE_DATES'
                        %msg               =  new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '003' " Enter Begin Date for travel
                                                           v1       = ls_travel-travelid
                                                           severity = if_abap_behv_message=>severity-error )
                        %element-begindate = if_abap_behv=>mk-on ) TO reported-travel.
      ENDIF.
      IF ls_travel-enddate IS INITIAL.
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.

        APPEND VALUE #( %tky               = ls_travel-%tky
                        %state_area        = 'VALIDATE_DATES'
                        %msg               =  new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '004' " Enter EndDate for travel
                                                           v1       = ls_travel-travelid
                                                           severity = if_abap_behv_message=>severity-error )
                        %element-enddate   = if_abap_behv=>mk-on ) TO reported-travel.
      ENDIF.

      IF ls_travel-enddate < ls_travel-begindate AND ls_travel-begindate IS NOT INITIAL
                                                 AND ls_travel-enddate IS NOT INITIAL.

        failed-travel = VALUE #( BASE failed-travel ( %tky = ls_travel-%tky ) ).

        reported-travel = VALUE #( BASE reported-travel
                                   (  %tky = ls_travel-%tky
                                      %state_area  = 'VALIDATE_DATES'
                                      %msg = new_message(  id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '001' "Begin date &1 must not be after end date &2 for travel &3
                                                           v1       = ls_travel-begindate
                                                           v2       = ls_travel-enddate
                                                           v3       = ls_travel-travelid
                                                           severity = if_abap_behv_message=>severity-error )


                                       %element-begindate = if_abap_behv=>mk-on
                                       %element-enddate   = if_abap_behv=>mk-on
                                    )
                                 ).

      ELSEIF ls_travel-begindate < cl_abap_context_info=>get_system_date( )
        AND ls_travel-begindate IS NOT INITIAL AND ls_travel-enddate IS NOT INITIAL.

        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.

        APPEND VALUE #( %tky           = ls_travel-%tky
                        %state_area    = 'VALIDATE_DATES'
                        %msg           = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                      number   = '002' "Begin date &1 must be on or after system date
                                                      v1       = ls_travel-BeginDate
                                                      severity = if_abap_behv_message=>severity-error )
                        %element-begindate = if_abap_behv=>mk-on ) TO reported-travel.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

  METHOD setinitialstatus.

    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        UPDATE SET FIELDS
        WITH VALUE #( FOR key IN keys ( %tky          = key-%tky
                                        overallstatus = travel_status-open ) )
    REPORTED DATA(lt_reported).

    reported = CORRESPONDING #( DEEP lt_reported ).

  ENDMETHOD.


  METHOD accepttravel.

    "Modify travel instance
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        UPDATE FIELDS ( overallstatus )
        WITH VALUE #( FOR key IN keys ( %tky          = key-%tky
                                        overallstatus = travel_status-accepted ) )
    FAILED failed
    REPORTED reported.

    "Read changed data for action result
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        ALL FIELDS WITH
        CORRESPONDING #( keys )
      RESULT DATA(lt_travel).

    result = VALUE #( FOR travel IN lt_travel ( %tky   = travel-%tky
                                                %param = travel ) ).

  ENDMETHOD.

  METHOD rejecttravel.

    "Modify travel instance
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        UPDATE FIELDS (  overallstatus )
        WITH VALUE #( FOR key IN keys ( %tky          = key-%tky
                                        overallstatus = travel_status-rejected ) )
    FAILED failed
    REPORTED reported.

    "Read changed data for action result
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
      ENTITY travel
        ALL FIELDS WITH
        CORRESPONDING #( keys )
      RESULT DATA(lt_travel).

    result = VALUE #( FOR travel IN lt_travel ( %tky   = travel-%tky
                                                %param = travel ) ).

  ENDMETHOD.

  METHOD settravelid.

    "Read travel entity
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
     ENTITY travel
       FIELDS ( travelid )
       WITH CORRESPONDING #( keys )
     RESULT DATA(lt_travel).

    " remove lines where TravelID is already filled.
    DELETE lt_travel WHERE travelid IS NOT INITIAL.
    CHECK lt_travel IS NOT INITIAL.

    " Select max travel ID
    SELECT SINGLE
        FROM  /dmo/a_travel_d
        FIELDS MAX( travel_id )
        INTO @DATA(max_travelid).

    " Set the travel ID
    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
    ENTITY travel
      UPDATE
        FROM VALUE #( FOR ls_travel IN lt_travel INDEX INTO i (
          %tky              = ls_travel-%tky
          travelid          = max_travelid + i
          %control-travelid = if_abap_behv=>mk-on ) )
    REPORTED DATA(update_reported).

    reported = CORRESPONDING #( DEEP update_reported ).

*    " Commented out as several number range object makes no sence
*    "when students use identical DB table
*    LOOP AT lt_travel ASSIGNING FIELD-SYMBOL(<ls_travel>) WHERE travelid IS INITIAL.
*
*      CALL FUNCTION 'NUMBER_GET_NEXT'
*        EXPORTING
*          nr_range_nr             = '01'
*          object                  = 'ZRAPH_TRID'
*        IMPORTING
*          number                  = <ls_travel>-travelid
*        EXCEPTIONS
*          interval_not_found      = 1
*          number_range_not_intern = 2
*          object_not_found        = 3
*          quantity_is_0           = 4
*          quantity_is_not_1       = 5
*          interval_overflow       = 6
*          buffer_overflow         = 7
*          OTHERS                  = 8.
*
*    ENDLOOP.
*
*    MODIFY ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
*      ENTITY travel
*        UPDATE FIELDS ( travelid )
**        WITH VALUE #( FOR key IN keys ( %tky          = key-%tky
**                                        OverallStatus = travel_status-open ) )
*      WITH CORRESPONDING #( lt_travel )
*    REPORTED DATA(lt_reported).
*
*    reported = CORRESPONDING #( DEEP lt_reported ).

  ENDMETHOD.


  METHOD validatecustomer.
    " Read relevant travel instances
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
             ENTITY travel
               FIELDS ( customerid )
               WITH CORRESPONDING #( keys )
           RESULT DATA(lt_travel)
           FAILED DATA(lt_failed).

    " Forward failed instances
    failed = CORRESPONDING #( DEEP lt_failed ).

    " Get Customer Master Data for comparison
    SELECT FROM /dmo/customer AS customer
    INNER JOIN @lt_travel AS travel ON travel~customerid = customer~customer_id
    FIELDS customer_id
    INTO TABLE @DATA(lt_customer_db).


    " Loop over all travel instances to be saved
    LOOP AT lt_travel INTO DATA(ls_travel).
      APPEND VALUE #(  %tky               = ls_travel-%tky
                       %state_area        = 'VALIDATE_CUSTOMER' ) TO reported-travel.

      " Raise messages for empty customer ids
      IF ls_travel-customerid IS INITIAL.
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.

        APPEND VALUE #( %tky                = ls_travel-%tky
                        %state_area         = 'VALIDATE_CUSTOMER'
                        %msg                = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '005' " Customer is initial
                                                           severity = if_abap_behv_message=>severity-error )
                        %element-customerid = if_abap_behv=>mk-on ) TO reported-travel.

        " Raise messages for non existing customer ids
      ELSEIF ls_travel-customerid IS NOT INITIAL AND NOT line_exists( lt_customer_db[ customer_id = ls_travel-customerid ] ).
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.
        APPEND VALUE #( %tky                = ls_travel-%tky
                        %state_area         = 'VALIDATE_CUSTOMER'
                        %msg                = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                           number   = '006' " Customer &1 unknown
                                                           v1       = ls_travel-customerid
                                                           severity = if_abap_behv_message=>severity-error )
                        %element-customerid = if_abap_behv=>mk-on ) TO reported-travel.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.

  METHOD validateagency.
    " Read relevant travel instance data
    READ ENTITIES OF zraph_i_travelwdtp IN LOCAL MODE
             ENTITY travel
               FIELDS ( agencyid )
               WITH CORRESPONDING #( keys )
           RESULT DATA(lt_travel)
           FAILED DATA(lt_failed).

    " Forward failed instances
    failed = CORRESPONDING #( DEEP lt_failed ).

    " Get Agency Master Data for comparison
    SELECT FROM /dmo/agency AS agency
    INNER JOIN @lt_travel AS travel ON travel~agencyid = agency~agency_id
    FIELDS agency_id
    INTO TABLE @DATA(lt_agency_db).


    " Loop over all travel instances to be saved
    LOOP AT lt_travel INTO DATA(ls_travel).
      APPEND VALUE #(  %tky               = ls_travel-%tky
                       %state_area        = 'VALIDATE_AGENCY' ) TO reported-travel.

      " Raise messages for empty agency ids
      IF ls_travel-agencyid IS INITIAL.
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.
        APPEND VALUE #( %tky              = ls_travel-%tky
                        %state_area       = 'VALIDATE_AGENCY'
                        %msg              = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                         number   = '007' " Agency is initial
                                                         severity = if_abap_behv_message=>severity-error )
                        %element-agencyid = if_abap_behv=>mk-on ) TO reported-travel.

        " Raise messages for non existing agency ids
      ELSEIF ls_travel-agencyid IS NOT INITIAL AND NOT line_exists( lt_agency_db[ agency_id = ls_travel-agencyid ] ).
        APPEND VALUE #( %tky = ls_travel-%tky ) TO failed-travel.
        APPEND VALUE #( %tky              = ls_travel-%tky
                        %state_area       = 'VALIDATE_AGENCY'
                        %msg              = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                         number   = '008' " Agency &1 unknown
                                                         v1       = ls_travel-agencyid
                                                         severity = if_abap_behv_message=>severity-error )
                        %element-agencyid = if_abap_behv=>mk-on ) TO reported-travel.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.

  METHOD is_create_granted.
    AUTHORITY-CHECK OBJECT 'ZRAPH_STAT'
          ID 'ZRAPH_STAT' DUMMY
          ID 'ACTVT' FIELD '01'.
    create_granted = COND #( WHEN sy-subrc = 0 THEN abap_true ELSE abap_false ).
    " Simulate full access - for testing purposes only! Needs to be removed for a productive implementation.
    create_granted = abap_true.
  ENDMETHOD.

  METHOD is_delete_granted.
    IF has_before_image = abap_true.
      AUTHORITY-CHECK OBJECT 'ZRAPH_STAT'
        ID 'ZRAPH_STAT' FIELD overall_status
        ID 'ACTVT' FIELD '06'.
    ELSE.
      AUTHORITY-CHECK OBJECT 'ZRAPH_STAT'
        ID 'ZRAPH_STAT' DUMMY
        ID 'ACTVT' FIELD '06'.
    ENDIF.
    delete_granted = COND #( WHEN sy-subrc = 0 THEN abap_true ELSE abap_false ).

    " Simulate full access - for testing purposes only! Needs to be removed for a productive implementation.
    delete_granted = abap_true.
  ENDMETHOD.

  METHOD is_update_granted.
    IF has_before_image = abap_true.
      AUTHORITY-CHECK OBJECT 'ZZRAPH_STAT'
        ID 'ZRAPH_STAT' FIELD overall_status
        ID 'ACTVT' FIELD '02'.
    ELSE.
      AUTHORITY-CHECK OBJECT 'ZRAPH_STAT'
        ID 'ZRAPH_STAT' DUMMY
        ID 'ACTVT' FIELD '02'.
    ENDIF.
    update_granted = COND #( WHEN sy-subrc = 0 THEN abap_true ELSE abap_false ).

    " Simulate full access - for testing purposes only! Needs to be removed for a productive implementation.
    update_granted = abap_true.
  ENDMETHOD.

ENDCLASS.
