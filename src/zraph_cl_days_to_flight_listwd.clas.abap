CLASS zraph_cl_days_to_flight_listwd DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    "for calculate
    INTERFACES if_sadl_exit_calc_element_read.

    "for filter
*    INTERFACES if_sadl_exit_filter_transform.

    "for sorting
*    INTERFACES if_sadl_exit_sort_transform.

  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZRAPH_CL_DAYS_TO_FLIGHT_LISTWD IMPLEMENTATION.


  METHOD if_sadl_exit_calc_element_read~calculate.

*    DATA lt_original_data TYPE STANDARD TABLE OF ZRAPH_C_TravelWDTP WITH DEFAULT KEY.
*    lt_original_data = CORRESPONDING #( it_original_data ).
*
**    """"ERROR -->
**    READ ENTITIES OF zraph_i_travelwdtp
**    ENTITY travel BY \_Booking
**    FIELDS ( FlightDate )
**    WITH VALUE #( FOR ls IN lt_original_data ( %tky-TravelUUID = ls-TravelUUID ) )
**    RESULT DATA(lt_bookings).
*
*    SELECT FROM @lt_original_Data AS travel
*    INNER JOIN ZRAPH_I_BookingWDTP AS bk ON travel~TravelUUID = bk~TravelUUID
*    FIELDS travel~TravelUUID, MIN( bk~FlightDate ) AS MinFlightDate, travel~TravelID
*    WHERE FlightDate >= @sy-datum
*    GROUP BY travel~TravelUUID, travel~TravelID
*    ORDER BY travelid, MinFlightDate
*    INTO TABLE @DATA(lt_booking).
*
*
*    LOOP AT lt_original_data ASSIGNING FIELD-SYMBOL(<fs_original_data>).
*
*      DATA(ls_booking) = VALUE #( lt_booking[ TravelUUID = <fs_original_data>-TravelUUID ] OPTIONAL ).
*      CHECK ls_booking IS NOT INITIAL.
*      <fs_original_data>-NextFlightInDays =  ls_booking-minflightdate - sy-datum.
*
*    ENDLOOP.
*
*    ct_calculated_data = CORRESPONDING #(  lt_original_data ).

  ENDMETHOD.


  METHOD if_sadl_exit_calc_element_read~get_calculation_info.

    " SAP Example !
** IF iv_entity <> '/DMO/C_BOOKING_VE'.
**      RAISE EXCEPTION TYPE /dmo/cx_virtual_elements
**        EXPORTING
**          textid = /dmo/cx_virtual_elements=>entity_not_known
**          entity = iv_entity.
**    ENDIF.
*
*    LOOP AT it_requested_calc_elements ASSIGNING FIELD-SYMBOL(<fs_calc_element>).
**
*      CASE <fs_calc_element>.
*        WHEN 'DAYSTOFLIGHT'.
*          APPEND 'FLIGHTDATE' TO et_requested_orig_elements.
*
*
*        WHEN OTHERS.
**          RAISE EXCEPTION TYPE /dmo/cx_virtual_elements
**            EXPORTING
**              textid  = /dmo/cx_virtual_elements=>virtual_element_not_known
**              element = <fs_calc_element>
**              entity  = iv_entity.
*      ENDCASE.
*    ENDLOOP.


*    "SIMPLIFIED VERSION
*    DATA(lt_requested_elements) = VALUE if_sadl_exit_calc_element_read=>tt_elements( ( |_BOOKING.FLIGHTDATE| )
*     ).
*    INSERT LINES OF lt_requested_elements INTO TABLE et_requested_orig_elements.

  ENDMETHOD.
ENDCLASS.
