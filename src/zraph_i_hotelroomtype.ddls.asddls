@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'RAP HandsOn: Hotel Room Type (VH)'
@Search.searchable: true
define view entity ZRAPH_I_HotelRoomType
  as select from    DDCDS_CUSTOMER_DOMAIN_VALUE( p_domain_name: 'ZRAPH_ROOM_TYPE')   as FixedValue
    left outer join DDCDS_CUSTOMER_DOMAIN_VALUE_T( p_domain_name: 'ZRAPH_ROOM_TYPE') as ValueText on  FixedValue.domain_name = ValueText.domain_name
                                                                                                  and FixedValue.value_low   = ValueText.value_low
{
  key FixedValue.value_low as Value,
      @Semantics.text: true
      @Search.defaultSearchElement: true
      ValueText.text       as Text
}
